import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlKeysComponent } from './control-keys.component';

describe('ControlKeysComponent', () => {
  let component: ControlKeysComponent;
  let fixture: ComponentFixture<ControlKeysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlKeysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlKeysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
