#!/bin/bash

cd "$(dirname "$0")"

npm start &
sleep 40 &&
open 'http://localhost:4200/control' &&
open 'http://localhost:4200'

#keep the command window open
while true; do sleep 10000; done;
