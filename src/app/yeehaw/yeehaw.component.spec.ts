import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YeehawComponent } from './yeehaw.component';

describe('YeehawComponent', () => {
  let component: YeehawComponent;
  let fixture: ComponentFixture<YeehawComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YeehawComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YeehawComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
