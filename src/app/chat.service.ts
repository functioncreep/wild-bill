import { Injectable } from '@angular/core';
// import { Subject } from 'rxjs/Subject';
import { Observable, Subject } from 'rxjs/Rx';
import { WebsocketService } from './websocket.service';

@Injectable()
export class ChatService {

  public blackout: boolean = false;
  public messages: Subject<any>;
  public liveOn: boolean = false;

  private messagePayload: any;
  private tipSound: HTMLAudioElement;
  private chatSound: HTMLAudioElement;

  constructor(private websocketService: WebsocketService) {
    this.messages = <Subject<any>> websocketService
      .connect()
      .map((response: any) => {
        return response;
      });

    // Websocket message model
    this.messagePayload = {
      type: null,
      data: {}
    }
  }


  public sendMsg(msg) {
    this.messages.next(msg);
  }


  public nextMessage() {
    this.chatSound = new Audio();
    this.chatSound.src = '/assets/sounds/message.wav';
    this.chatSound.load();
    this.chatSound.play();
    this.messagePayload.type = 'chat';
    this.messagePayload.data = {};
    this.messages.next(Object.assign({}, this.messagePayload));
    console.log('next message =>', this.messagePayload);
    this.messagePayload.type = null;
  }


  public addViewers(amount: number) {
    this.messagePayload.type = 'viewers';
    this.messagePayload.data = { amount: amount };
    this.messages.next(Object.assign({}, this.messagePayload));
    this.messagePayload.type = null;
    this.messagePayload.data = {};
  }


  public addTip(amount: number) {
    this.tipSound = new Audio();
    this.tipSound.src = '/assets/sounds/spitoon.wav';
    this.tipSound.load();
    this.tipSound.play();
    this.messagePayload.type = 'tip';
    this.messagePayload.data = { amount: amount };
    this.messages.next(Object.assign({}, this.messagePayload));
    this.messagePayload.type = null;
    this.messagePayload.data = {};
  }


  public silentTip() {
    this.messagePayload.type = 'tip';
    this.messagePayload.data = { amount: 1 };
    this.messages.next(Object.assign({}, this.messagePayload));
    this.messagePayload.type = null;
    this.messagePayload.data = {};
  }


  public toggleLive() {
    this.liveOn = (this.liveOn) ? false : true;
    this.messagePayload.type = 'golive';
    this.messagePayload.data = { live: this.liveOn };
    this.messages.next(Object.assign({}, this.messagePayload));
    this.messagePayload.type = null;
  }


  public toggleBlackout() {
    this.messagePayload.type = 'blackout';
    this.messagePayload.data = {};
    this.messages.next(Object.assign({}, this.messagePayload));
    this.messagePayload.type = null;
  }

  public getAllMessages() {
    this.messagePayload.type = 'script';
    this.messagePayload.data = {};
    this.messages.next(Object.assign({}, this.messagePayload));
    this.messagePayload.type = null;
  }


  public resetAll() {
    this.liveOn = false;
    this.messagePayload.type = 'reset';
    this.messagePayload.data = {};
    this.messages.next(Object.assign({}, this.messagePayload));
    this.messagePayload.type = null;
  }


  public spitoonSound() {
    this.tipSound = new Audio();
    this.tipSound.src = '/assets/sounds/spitoon_tail.wav';
    this.tipSound.load();
    this.tipSound.play();
  }

}
