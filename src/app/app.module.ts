import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { WebcamModule } from 'ngx-webcam';
import { WebsocketService } from './websocket.service';
import { ChatService } from './chat.service';
import { ChatComponent } from './chat/chat.component';
import { WebcamComponent } from './webcam/webcam.component';
import { ControlComponent } from './control/control.component';
import { AppRoutingModule } from './/app-routing.module';
import { YeehawComponent } from './yeehaw/yeehaw.component';
import { ControlKeysComponent } from './control/control-keys/control-keys.component';
import { ScrollDirective } from './scroll.directive';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { LiveComponent } from './live/live.component';


@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    WebcamComponent,
    ControlComponent,
    YeehawComponent,
    ControlKeysComponent,
    ScrollDirective,
    LiveComponent,
  ],
  imports: [
    BrowserModule,
    WebcamModule,
    AppRoutingModule,
    ScrollToModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule
  ],
  providers: [
    WebsocketService,
    ChatService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
