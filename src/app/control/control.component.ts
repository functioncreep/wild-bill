import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChatService } from '../chat.service';
import { Subject } from 'rxjs';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';
import { createUrlResolverWithoutPackagePrefix } from '@angular/compiler';

@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.scss']
})
export class ControlComponent implements OnInit, OnDestroy {

  private unsubscribe = new Subject<void>();

  public allMessages = [];
  public currentMessage: number = 0;

  constructor(
    private chatService: ChatService,
    private scrollToService: ScrollToService
  ) {}


  ngOnInit() {
    this.chatService.messages
      .takeUntil(this.unsubscribe)
      .subscribe(msg => {
        console.log(msg);
        switch (msg.type) {
          case 'script':
            this.allMessages = msg.data;
            break;
          case 'chat':
            this.currentMessage = msg.data.index;
            console.log('currentMessage =>', this.currentMessage);
            this.triggerScrollTo();
            break;
        }
      });

    this.getAllMessages();
  }


  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }


  public nextMessage() {
    this.chatService.nextMessage();
  }


  public addViewers(amount: number) {
    console.log('add viewers');
    this.chatService.addViewers(amount);
  }

  
  public addTip(amount: number) {
    console.log('add tip');
    this.chatService.addTip(amount);
  }


  private silentTip() {
    this.chatService.silentTip();
  }


  public spitoonSound() {
    this.chatService.spitoonSound();
  }


  public toggleBlackout() {
    this.chatService.toggleBlackout();
  }


  public toggleLive() {
    this.chatService.toggleLive();
  }


  public getAllMessages() {
    this.chatService.getAllMessages();
  }


  private triggerScrollTo() {
    const config: ScrollToConfigOptions = {
      target: 'current',
      duration: 0
    };
    this.scrollToService.scrollTo(config);
  }

  public resetAll() {
    let confirmOnce = window.confirm('Are you sure you want to reset everything?');
    if (confirmOnce) {
      let confirmTwice = window.confirm('No, seriously... like, EVERYTHING?');
      if (confirmTwice) {
        this.chatService.resetAll();
      }
    }

  }
}
