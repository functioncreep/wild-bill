const express = require('express');
const app = express();
const http = require('http');
const mainServer = new http.Server(app);
const io = require('socket.io')(mainServer);

let scriptIndex = 0;
const chatScript = [];
const chatMessages = [];

app.use(['/control', '/'], express.static('./dist'));

io.on('connection', (socket) => {

    // Log whenever a user connects
    console.log('user connected');

    // Log whenever a client disconnects from our websocket server
    socket.on('disconnect', function(){
        console.log('user disconnected');
    });

    // When we receive a 'message' event from our client, print out
    // the contents of that message and then echo it back to our client
    // using `io.emit()`
    socket.on('message', (message) => {
        console.log("Message Received: " + message);

        if (message.hasOwnProperty('type') && message.hasOwnProperty('data')) {
            if (message.type === 'chat') {
                console.log("Chat Message Received: " + JSON.stringify(message));
                chatMessages[scriptIndex].index = scriptIndex;
                message.data = chatMessages[scriptIndex];
                io.emit('message', message);
                scriptIndex ++;
                scriptIndex %= chatMessages.length;
            } else if (message.type === 'script') {
                console.log("Sending Script: " + JSON.stringify(message));
                message.data = chatMessages;
                io.emit('message', message);
            } else if (message.type === 'reset') {
                scriptIndex = 0;
                io.emit('message', message);
            } else {
                console.log("Message Received: " + JSON.stringify(message));
                io.emit('message', message);
            }
        }    
    });
});

mainServer.listen(4200, err => {
    if (err) {
        console.log(err);
    } else {
        console.log(`started on port 4200`);
    }
});

// Read chat script
const readline = require('readline');
const fs = require('fs');
const script = [];

const rl = readline.createInterface({
    input: fs.createReadStream('chat_file.txt')
});

rl.on('line', function (line) {
    // console.log('Script:', line);
    chatScript.push(line);
});

rl.on('close', function () {
    console.log ('Finished reading script!');
    const chatMessage = {
        username: null,
        text: null,
        index: null
    };

    for (let i=0; i<chatScript.length; i++) {
        let lineParsed = chatScript[i].split(' ');

        if (lineParsed[0] === '/chat') {
            if (chatMessage.username && chatMessage.text) {
                // console.log(chatMessage);
                chatMessages.push(Object.assign({}, chatMessage));
            }
            chatMessage.username = lineParsed[1];
        } else {
            chatMessage.text = chatScript[i];
        }
    }
});