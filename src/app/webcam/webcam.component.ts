import { Component, OnInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { ChatService } from '../chat.service';

@Component({
  selector: 'app-webcam',
  templateUrl: './webcam.component.html',
  styleUrls: ['./webcam.component.scss']
})
export class WebcamComponent implements OnInit, OnDestroy {

  @ViewChild('webcam') webcam: ElementRef;

  private unsubscribe = new Subject<void>();
  
  public viewers: number = 0;
  public liveString = 'Go Live!';
  private suspendToggle: boolean = false;

  constructor(private chatService: ChatService) { }

  ngOnInit() {
    console.log('webcam =>', this.webcam);

    this.chatService.messages
      .takeUntil(this.unsubscribe)
      .subscribe(msg => {
        console.log('chat message =>', msg);
        switch (msg.type) {
          case 'viewers':
            this.viewers = (this.viewers + msg.data.amount > 0) ? this.viewers += msg.data.amount : 0;
            break;
          case 'golive':
            this.liveString = (msg.data.live) ? 'LIVE' : 'Go Live!';
            this.chatService.liveOn = (msg.data.live) ? true : false;
            break;
          case 'reset':
            this.resetAll();
            break;
        }
      });
  }


  ngAfterViewInit() {
    // Some style fixes to video size
    const webcamElement = this.webcam.nativeElement.children[0];
    const wrapperElement = webcamElement.children[0];
    const videoElement = wrapperElement.children[0];

    // console.log('webcamElement =>', webcamElement);
    // console.log('wrapperElement =>', wrapperElement);
    // console.log('videoElement =>', videoElement);

    // wrapperElement.classList.add('fullwidth');
    // videoElement.classList.add('fullwidth');

    wrapperElement.style.width = '100%';
    videoElement.style.width = '100%';
  }


  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }


  public toggleLive() {
    this.liveString = (this.chatService.liveOn) ? 'Go Live!' : 'LIVE';
    this.chatService.toggleLive();
  }


  private resetAll() {
    this.viewers = 0;
    this.liveString = 'Go Live!';
  }

}
