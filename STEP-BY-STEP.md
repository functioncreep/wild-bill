# Simple Step-by-Step Mac OS Installation Instructions for Non-Nerds

1. Download and install node.js 8+
    - Version 8.17 should work, and [can be downloaded here](https://nodejs.org/download/release/v8.17.0/node-v8.17.0.pkg).
    - In case there are problems running the project with this version, try a previous 8.0+ version (e.g. 8.16), which can be found at https://nodejs.org/en/download/releases/ (type `node.js 8` into the search bar to locate them).

2. Open the Mac terminal from the wild bill project folder, or navigate there in the terminal.
    - See [How to Launch Terminal in the Current Folder Location on Mac](https://www.maketecheasier.com/launch-terminal-current-folder-mac/)

3. Verify that node.js and npm are working:
    - Type `node -v` and hit `Enter`. You should see `v8.17.0` (or whichever version you previously installed).
    - Type `npm -v` and hit `Enter`. You should see `6.13.4` (or whichever version you previously installed).

4. If the last step worked without issue, you should be feeling like you're Neo in the Matrix, or at least some sort of badass hacker. 😎 Now use those sweet hacking skillz to type `npm i` or `npm install` if you're not feeling lazy. And don't forget to hit `Enter`.

5. Now you should be seeing a bunch of text doing a bunch of stuff on the screen. Let it do its thing for a bit, and when it's done (you'll know it's done when nothing's moving/animating and you can see the terminal prompt and cursor for typing again), take a look to see if there's any mention of an error or failure to install in the terminal screen. If not, all should be well! Otherwise, you might want to start from step 1 again with a slightly different node 8 version, or call Chris and yell at him until he fixes it.

6. If the last step went smooth as butter then go ahead and minimize the terminal, and try running the `WILD-BILL-LIVE.command` file.
    - If you get an error about not having permission to run this file, you may have to put your cool hacker shoes back on, and get back on that terminal to run a quick command to fix it (see [this page](https://support.apple.com/en-gb/guide/terminal/apdd100908f-06b3-4e63-8a87-32e71241bab4/mac) for instructions... In this case, if you're already in the wild bill project directory, you can skip the `cd` command in the first step, and the command in the second step will look like `chmod 755 WILD-BILL-LIVE.command` ).
    - If above doesn't work to fix the permission error, call Chris, yell, etc. etc. 😉
    - In case the file does run but no web browser opens up, try opening one manually and navigating to `http://localhost:4200` **or** `http://127.0.0.1:4200`. 