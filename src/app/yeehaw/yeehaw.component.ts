import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChatComponent } from '../chat/chat.component';
import { WebcamComponent } from '../webcam/webcam.component';
import { ChatService } from '../chat.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-yeehaw',
  templateUrl: './yeehaw.component.html',
  styleUrls: ['./yeehaw.component.scss']
})
export class YeehawComponent implements OnInit, OnDestroy {

  public blackout: boolean = false;

  private unsubscribe = new Subject<void>();

  constructor(private chatService: ChatService) { }

  ngOnInit() {
    this.chatService.messages
      .takeUntil(this.unsubscribe)
      .subscribe(msg => {
        console.log('chat message =>', msg);
        switch (msg.type) {
          case 'blackout':
            this.blackout = (this.blackout) ? false : true;
            break;
        }
      });
  }


  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

}
