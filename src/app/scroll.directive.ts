import { Directive, ElementRef, Input, HostListener } from '@angular/core';

@Directive({
  selector: '[scrollButton]'
})
export class ScrollDirective {

  // this.chatDiv: any;

  constructor(private el: ElementRef) {}

  // @HostListener('this.parentElement:scroll') onParentScroll() {
  //   this.chatDiv = this.el.nativeElement.parentElement;
  //   this.chatDiv.onscroll = function(event) {
  //     if ( (this.chatDiv.scrollTop + this.chatDiv.offsetHeight) >= this.chatDiv.scrollHeight - 50 ) {
  //         autoScroll = true;
  //         jumpButton.style.display = 'none';
  //     } else {
  //         autoScroll = false;
  //         jumpButton.style.display = 'block';
  //     }
  //   }
  // }

  @HostListener('click') onclick() {
    console.log(this.el);

    // this.chatDiv.scrollTop = this.chatDiv.scrollHeight;
    this.el.nativeElement.style.display = 'none';
  }

}
