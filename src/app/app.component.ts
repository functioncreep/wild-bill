import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public title = 'app';

  constructor() {}


  ngOnInit() {
    console.log('ws_url =>', environment.ws_url);
  }


  @HostListener('window:keydown', ['$event'])
  onKeydown(event) {
    console.log(event);
  } 
}
